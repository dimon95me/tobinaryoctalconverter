package examplesoureit.convetertobinary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.first_number)
    EditText firstNumber;
    @BindView(R.id.second_number)
    EditText secondNumber;
    @BindView(R.id.third_number)
    EditText thirdNumber;

    @BindView(R.id.binary)
    EditText binary;
    @BindView(R.id.octal)
    EditText octal;
    @BindView(R.id.hexadecimal)
    EditText hexadecimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnTextChanged(R.id.first_number)
    public void toBinary() {
        try {
            binary.setText(Integer.toBinaryString(Integer.parseInt(firstNumber.getText().toString())));
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Too big number", Toast.LENGTH_SHORT).show();
        }
    }

    @OnTextChanged(R.id.second_number)
    public void toOctal() {
        try {
            octal.setText(Integer.toOctalString(Integer.parseInt(secondNumber.getText().toString())));
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Too big number", Toast.LENGTH_SHORT).show();
        }
    }

    @OnTextChanged(R.id.third_number)
    public void toHexadecimal(){
        try {
            hexadecimal.setText(Integer.toHexString(Integer.parseInt(thirdNumber.getText().toString())));
        } catch (NumberFormatException e){
            Toast.makeText(this, "Too big number", Toast.LENGTH_SHORT).show();
        }
    }
}
